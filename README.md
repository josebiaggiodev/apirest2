# API Rest Two

# Sobre o projeto

Uma API Rest que consiste em: buscar por todos os jogos, buscar por um determinado jogo, buscar por todas as listas de jogos ou buscar por todos os jogos de uma determinada lista de jogos. 

# Diagrama de classes em UML

![Diagrama](https://gitlab.com/josebiaggiodev/assets/-/raw/main/classDiagramAPIRestTwo.png)

# Endpoints da API

- /games [GET] - Buscar por todos os jogos

![/games](https://gitlab.com/josebiaggiodev/assets/-/raw/main/getGamesAPIRestTwo.png)

- /games/{id} [GET] - Buscar por um determinado jogo

![/games/{id}](https://gitlab.com/josebiaggiodev/assets/-/raw/main/getGamesByIdAPIRestTwo.png)

- /lists [GET] - Buscar por todas as listas de jogos

![/lists](https://gitlab.com/josebiaggiodev/assets/-/raw/main/getListsAPIRestTwo.png)

- /lists/{listId}/games [GET] - Buscar por todos os jogos de uma determinada lista de jogos

![/lists/{listId}/games](https://gitlab.com/josebiaggiodev/assets/-/raw/main/getGamesByListsAPIRestTwo.png)

# Tecnologias

- Java 17
- Spring Boot 3.0.6
- Maven 4.0.0
- H2 Database

# Autor

José Biaggio
