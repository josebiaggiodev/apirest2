package com.josebiaggiodev.apirest2.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.josebiaggiodev.apirest2.entities.GameList;

public interface GameListRepository extends JpaRepository<GameList, Long> {

}